fun main(){
    var  count = 1
    while (count <= 100){
        if (count.rem(3) == 0 && count.rem(5) == 0) {
            println("FizzBuzz")                                         //Multiples of both 3 and 5
        }else if (count.rem(3) == 0){
            println("Fizz")                                             //Multiples of 3
        } else if (count.rem(5) == 0){
            println("Buzz")                                             //Multiples of 5
        } else if (count.rem(2) != 0){
            println("Prime")                                            //Prime numbers
        } else{
            println("$count")
        }
        count++
    }
}